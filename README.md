Potato Leaf Disease Classification Using SVM 

This project aims to detect and classify potato leaf diseases using machine learning techniques. The project involves data collection, preprocessing, model training, and evaluation.


## Introduction

Potato leaf diseases can significantly impact potato crop yield and quality. Early detection of these diseases is crucial for taking timely action to prevent their spread. This project uses image processing and machine learning techniques to identify and classify common potato leaf diseases.

## Dataset

The dataset used in this project consists of images of potato leaves affected by different diseases as well as healthy leaves. The dataset can be obtained from [Kaggle](https://www.kaggle.com/datasets) or other open-source repositories.

### Dataset Details

- **Healthy Leaves**
- **Diseased Leaves**
  - Early Blight
  - Late Blight
  - Other diseases

## Installation
To run this project, you need to have Python and several libraries installed. The recommended way to install the required libraries is by using a virtual environment.

1. Clone the repository:

   ```bash
   git clone https://github.com/yourusername/potato-leaf-disease-detection.git
   cd potato-leaf-disease-detection


2. Create a virtual environment and activate it:
python3 -m venv env
source env/bin/activate   # On Windows use `env\Scripts\activate`


3. Install the required libraries:
pip install -r requirements.txt

4. Run the code
python app.py


   ```bash
   git clone https://github.com/yourusername/potato-leaf-disease-detection.git
   cd potato-leaf-disease-detection

## Deployed Link
https://leaf-disease-classify.onrender.com/