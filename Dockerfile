FROM python:3.12

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1
# ENV PYTHONPATH \Users\DELL\OneDrive\Desktop\ml

ENV  SERVER_ADDRESS http://localhost:5000/

# Set the PYTHONPATH environment variable to include the "ml" directory
ENV PYTHONPATH /app/ml
# Set the working directory in the container
WORKDIR /app

COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# COPY transformers.py .
COPY env.sample .env
COPY . .

CMD ["gunicorn","--bind","0.0.0.0:5000","app:app"]
